---
title: About Bitrinsics
subtitle: Providing measurement and inference of the fundamental value drivers behind the crypto economy
comments: false
---

Bitrinsics relies on the Coinward Rating Engine for real-time data that provides the basis for modeling network value and identifying investment and trading opportunities.



