---
title: Valuing Networks
subtitle: How to Think About Cryptoasset Networks (updated 6/19)
date: 2018-05-04
---

**Any group or system of interconnected people can and should be viewed as a network.**

Bitcoin is a Network, Bitcoin (BTC) is its Currency, Proof of Work is the Protocol, SHA-256 is the Production Method

Ethereum is a Network, Ether (ETH) is its Currency, Proof of Work is the Protocol, Ethhash is the Production Method

Litecoin is a Network, Litecoin (LTC) is its Currency, Proof of Work is the Protocol, Scrypt is the Production Method

Bitcoin Cash is a Network, Bcash (BCH) is its Currency, Proof of Work is the Protocol, SHA-256 is the Production Method

Monero is a Network, Monero (XMR) is its Currency, Proof of Work is the Protocol, CryptoNight is the Production Method

United States of America is a Network, US Dollar (USD) is its Currency, Fiat is the Protocol, Printing is the Production Method

Facebook is a Network, Libra is its Currency, Proof of Stake is the Protocol, the Production Method is TBD in 2020

| Network | Currency | Protocol | Production Method
| :------ |:--- | :--- | :---
| Bitcoin | Bitcoin (BTC) | Proof of Work | SHA 256 |
| Ethereum | Ether (ETH) | Proof of Work | Ethhash |
| Litecoin | Litecoin (LTC) | Proof of Work | Scrypt |
| Bitcoin Cash | Bcash (BCH) | Proof of Work | SHA 256 |
| Monero | Monero (XMR) | Proof of Work | CryptoNight
| United States of America | US Dollar (USD) | Fiat | Fancy Printing Press
| Facebook's Libra Network | Libra | Proof of Stake | TBD
 
The US Dollar derives the majority of its value from sanction by a government with massive political and military power subsystems. This sanction has motivated independent actors to create and grow supporting subsystems for economic gain, allowing those within the USA access to strong currency that carries purchasing power across many networks. Thanks DC!

None of the cryptography-backed networks have such a luxury.

Instead, these networks such as bitcoin often rely on hard coded, transparent supply limits because they provide the network with a high degree of certainty. Certainty bears intrinsic value. Fiat lacks this feature.

Successful cryptoasset networks source value from 6 critical subsystems.

The 1st subsystem is Clients, defined by the software that is built to interact with the network. An example of this is an explorer client, or wallet like Mycelium Bitcoin Wallet or Metamask Ethereum Extension. The growth of this subsystem can be measured by tracking functional clients supporting the network.

The 2nd subsystem is Ownership, defined by network participants holding their private keys. An example of ownership component is my friend Joe. The growth of this subsystem can be measured by tracking unique individuals holding their private keys.

The 3rd subsystem is Exchange, defined by third parties facilitating currency trading. An example exchange component is Binance. The growth of this subsystem can be measured by tracking listed pairs of the network's native currency.

The 4th subsystem is Commerce, defined by third parties facilitating the sale of material goods and services paid for in the network's native currency. An example of Commerce component is Overstock.com. The growth of this subsystem can be measured by tracking sales volumes.

The 5th subsystem is Development, defined by contributions and maintenance of the source code. An example of a Development Component is the Bitcoin Core Repository on Github. The growth of this subsystem can be measured by tracking developer commits.

The 6th subsystem is Production, defined by the tasks required to maintain and update the network. An example of a Production Component is an operational node, often setup with a mining rig. The growth of this subsystem can be measured by tracking nodes.

A network that has a robust development environment (5th) and ownership subsystem (2nd) yet lacks exchange support (3rd), functional clients(1st), and miners for production (6th) will have a diminished value compared to similar networks with all of those missing pieces.

An important omission that is often touted as an indicator of crypto network success is community member totals. This is a pure vanity metric that is easily gamed, as members can be purchased outright on platforms such as Telegram, a popular messaging platform for crypto projects. I do not intend to discredit the value in building a community, rather suggest the actual value is created once that community member decides to participate in one of the 6 subsystems, most commonly the subsystem with the shallowest learning curve; ownership.

Robert Metcalfe defined the size of a network as the number of nodes squared. While size is a useful metric for network comparison, my efforts are intended to explain the value of a network. Therefore, simply looking at nodes provides only a part of the picture.

Whenever considering currencies to use, the option with maximum purchasing power within the network you find yourself in is the obvious solution.

If you’re eating at a restaurant in the USA, you use US Dollars to pay the bill.

If that restaurant offers a program with their own currency, FoodBucks, that carry 20% more purchasing power at all the restaurants in their program’s network, you may find yourself considering converting some dollars to FoodBucks.

A second example might be a form of credit that gives you Cash Back or Points for use within their network. These credit options offer increased purchasing power.

Built in and perceived incentives lead to network churn, which is simply the movement of participants across networks.

**The overall growth of a network increases the purchasing power of its native currency, increasing the value of the network.**

Bitcoin was not the first currency native to an internet network. See Hashcash or Ecash

However, as of this writing, the Bitcoin network is the most valuable of those native to the internet. Since Satoshi created Bitcoin the currency in Jan 2009, the Bitcoin network has grown to the point it is today. The more people that understand what these networks are and how to participate, the more they grow.

If you are holding a percentage of your wealth in the form of a currency, it is likely the one that carries the most purchasing power within your current network. Today, 1 Bitcoin carries enough purchasing power for a few months of housing in my current network.

The finite supply combined with the potential for the network and demand for its currency to grow in future has been the backbone for the Bitcoin as a Store of Value argument. Other networks have used this feature for their currency.

Some other technical use cases such as Medium of Exchange and Unit of Account make a lot of sense. Humans will likely come up with others from here.

One example that did not exist 5 years ago is Initial Coin Offerings via programmable contracts, a financing mechanism for entrepreneurs which gives investors the ability to purchase a currency as a direct investment in a network, thus far creating a brand new economy for crypto capitalists moving their bitcoin or similar assets into the production systems they deem profitable. In 2019 anyone can create a sound and transparent currency for their network and that will likely continue to be a popular thing to do.

These currencies are all money. Using money as a descriptor in the same way you might say “Wow, Curry’s jump shot was money last night!”, where money implies a quality of reliable desirability, the value of the network determines how money the currency really is.

Forward, Onward, Coinward!