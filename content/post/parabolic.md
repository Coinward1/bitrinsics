---
title: Parabolic Advances
subtitle: A History of Bitcoin Price Increases
date: 2019-06-19
--- 
 
 Dates | Low | High | Advance
| :------ |:--- | :--- | :---
| Jul 2010 - Jan 2011 | $0.05 | $24.15 | 483x |
| Oct 2011 - Nov 2013 | $ 2 | $1,142 | 571x |
| Jan 2015 - Dec 2017 | $164 | $19,531 | 119x |
| Oct 2011 - Dec 2017 | $2 | $19,531 | 9,765x |
| Dec 2018 - Present | $3,148 | ???? |  
| Comparisons | | | |
| DJIA 1903 - Present | 30.88 | 26,952 | 873x |
| AAPL 1982 - Present | $0.16 | $232 | 1,448x |
| AMZN 1997 - Present | $1.31 | $2,050 | 1,564x |
|Gold - 1919 - Present | $20.67 | $1,920 | 93x |